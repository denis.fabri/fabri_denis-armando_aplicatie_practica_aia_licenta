from flask import Flask, render_template, jsonify, redirect, url_for
import serial

app = Flask(__name__)
ser = serial.Serial('/dev/ttyACM0', 9600)

def wait_for_serial_communication():
    if ser.in_waiting > 0:
        received_char = ser.read().decode('utf-8').strip()
        print(f"Received character: {received_char}")  # Print the received character
        if received_char == 'r':
            return 'notification'  # Return 'notification' if 'r' is received
        else:
            return 'noone'

@app.route('/')
def index():
    return render_template('index.html')

@app.route('/check_notification')
def check_notification():
    result = wait_for_serial_communication()
    if result == 'notification':
        return jsonify(page='notification')
    else:
        return jsonify(page='noone')

@app.route('/notification')
def notification():
    return render_template('notification.html')

@app.route('/noone')
def noone():
    return render_template('noone.html')

@app.route('/accept')
def accept():
    print(f"Sent character: a")
    ser.write(b'a')  # Send 'a' to Arduino as accept response
    return redirect(url_for('index'))

@app.route('/decline')
def decline():
    print(f"Sent character: d")
    ser.write(b'd')  # Send 'd' to Arduino as decline response
    return redirect(url_for('index'))

if __name__ == '__main__':
    app.run(host='0.0.0.0', port=5000, debug=False)
