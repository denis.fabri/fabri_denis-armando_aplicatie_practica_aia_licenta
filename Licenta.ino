#include <Adafruit_Fingerprint.h>
#include <Servo.h>
#include <Keypad.h>
#include <LiquidCrystal_I2C.h>
#include <EEPROM.h>

SoftwareSerial mySerial(2, 3);

LiquidCrystal_I2C lcd(0x27, 16, 2); //Create lcd object to print onto LCD using I2C adapter
Adafruit_Fingerprint finger = Adafruit_Fingerprint(&mySerial);
int pos = 1;
static int offset = 0;
int offset2 = 1;
char PIN[5] = "";
int id;
Servo myservo;

String line1 = " Bine ati venit!"; // Linie de mesaj stationară
String line2 = "Pentru acces: A-Folosire amprenta, B-Folosire Pin, C-Remote acces! "; // Linie de mesaj repetitivă

int Secure_fan = 0;

int screenWidth = 16;
int screenHeight = 2;

int stringStart, stringEnd = 0;
int scrollCursor = screenWidth; 

const byte ROWS = 4; //Patru rânduri
const byte COLS = 4; //Patru coloane
//Definirea simbolurilor pentru tastatură
char hexaKeys[ROWS][COLS] = {
  {'1','2','3','A'},
  {'4','5','6','B'},
  {'7','8','9','C'},
  {'*','0','#','D'}
};
byte rowPins[ROWS] = {12, 10, 9, 8}; //Pini cu rândurile tastaturii pentru conectare
byte colPins[COLS] = {7, 6, 5, 4}; //Pini cu coloanele tastaturii pentru conectare

//Inițializarea unei Instanțe a clasei de tip Keypad
Keypad customKeypad = Keypad( makeKeymap(hexaKeys), rowPins, colPins, ROWS, COLS); 

unsigned long lastActionMillis = 0;  
unsigned long currentMillis;
int secunde, cnt_sec = 1;
int adresa_pin_incep = 0;
int adresa_pin_sf = 4;
int adresa_nr_amprente = 5;
int numar_amprente;

void Get_Pin_EEPROM()
{
    uint8_t value = EEPROM.read(adresa_pin_incep);
    if(value == 255)
    {
      for (int i = adresa_pin_incep; i < adresa_pin_sf; i++) 
      {
        char c = '0';
        uint8_t asciiValue = (uint8_t)c;
        EEPROM.write(i, asciiValue);
        PIN[i] = '0';
      }
    }
    else
    {
      for (int i = adresa_pin_incep; i < adresa_pin_sf; i++) 
      {
        PIN[i] = EEPROM.read(i);
      }
    }
}

void Write_Pin_EEPROM(char pin[])
{
    for (int i = adresa_pin_incep; i <= adresa_pin_sf; i++) {
    EEPROM.write(i, pin[i]);
    Serial.print(pin[i]);
  }
}

void setup() 
{
  Serial.begin(9600);
  myservo.attach(11);  // attaches the servo on pin 9 to the servo object
  myservo.write(pos);              // tell servo to go to position in variable 'pos'
  delay(500);                       // waits 15ms for the servo to reach the positio      
  myservo.detach();
  
  lcd.init(); //initialize the lcd
  lcd.backlight(); //open the backlight
  lcd.setCursor(0, 0);
  
  customKeypad.addEventListener(keypadEvent);
  finger.begin(57600);
  delay(5);
  Get_Pin_EEPROM();
  EEPROM.get(adresa_nr_amprente, numar_amprente);
}

void loop() 
{
  currentMillis = millis();
  if((Secure_fan == 1) && (lastActionMillis != 0)) 
    {

      if((currentMillis - lastActionMillis) / 1000 == cnt_sec)
      {
        secunde = 30 - cnt_sec;
        lcd.clear();
        lcd.setCursor(0, 0);
        lcd.print("Va rog asteptati");
        lcd.setCursor(0, 1);
        lcd.print(secunde);
        lcd.print(" sec.");
        cnt_sec++;
        if(cnt_sec == 30)
        {
          Secure_fan = 0;
          lastActionMillis = 0;
          cnt_sec = 0;
        }
      }
    }
  else
  {
    if(currentMillis - lastActionMillis > 5000 && lastActionMillis != 0)
    {
      lcd.clear();
      lastActionMillis = 0;
      offset = 0;
      pos = 1;
      myservo.attach(11);  // attaches the servo on pin 9 to the servo object
      myservo.write(pos); 
      delay(500);                       // waits 15ms for the servo to reach the positio      
      myservo.detach(); 
    }
    else
    {
      updateLCD();
    }
      char customKey = customKeypad.getKey();
  }
}

void keypadEvent(KeypadEvent key)
{
    switch (customKeypad.getState()){
    case PRESSED:
          switch (key)
          {
            case 'A':
            Amprenta();
            break;
            case 'B':
            Get_Pin_EEPROM();
            Introducere_pin();
            break;
            case 'C':
            Remote_access();
            break;
            case '*':
            Schimbare_pin();
            break;
            case '#':
            AD_Amprenta();
            break;
            default:
            break;        
           }
        break;

    case RELEASED:
        break;

    case HOLD:
        break;
    }
}



// Returnează -1 dacă amprenta citită nu este gasită, in caz contrar returneză ID-ul amprentei
int getFingerprintIDez() {
  uint8_t p = finger.getImage();
  if (p != FINGERPRINT_OK)  return -1;

  p = finger.image2Tz();
  if (p != FINGERPRINT_OK)  return -1;

  p = finger.fingerFastSearch();
  if (p != FINGERPRINT_OK)  return -1;

  //Am găsit o asemănare
  return finger.fingerID;
}


void Amprenta()
{
  lcd.clear();
  lcd.setCursor(0, 0);
  lcd.print("Apropiati");
  lcd.setCursor(0, 1);
  lcd.print("degetul!");
  while(1)
  {
  if(getFingerprintIDez()!= -1)
  {
      if(pos == 1)
      {
        pos = 45;
        myservo.attach(11);  // attaches the servo on pin 9 to the servo object
        myservo.write(pos);              // tell servo to go to position in variable 'pos'
        delay(500);                       // waits 15ms for the servo to reach the positio      
        myservo.detach();             // tell servo to go to position in variable 'pos'
        lcd.clear();
        lcd.setCursor(0, 0);
        lcd.print("Deschis!");
        delay(2000);
        lastActionMillis = millis();
        break;
      }
  }
  delay(50);
  }
}

void Introducere_pin()
{
  char customK;
  int i,j;
  char pin[5] = "";
      for(i=0; i<3; i++)
      {
        lcd.clear();
        lcd.setCursor(0, 0);
        lcd.print("Introduceti");
        lcd.setCursor(0, 1);
        lcd.print("pin: ");
        j=0;
        while(j<4)
        {
          do
          {
            customK = customKeypad.waitForKey();
          }while(isdigit(customK != 0 ));
          lcd.print(customK);
          pin[j] = customK;
          j++;
        }
        delay(500);
        //Serial.println(pin);
        if(strcmp(pin,PIN)== 0)
        {
          lcd.clear();
          lcd.setCursor(0, 0);
          lcd.print("Pin corect!");
          delay(1000);
          if(pos == 1)
          {
            lcd.clear();
            lcd.setCursor(0, 0);
            pos = 45;
            myservo.attach(11);  // Atașează obiectul servomotor la pinul 11
            myservo.write(pos);              // Mută servomotorul la poziția 'pos'
            delay(500);                       // Așteaptă 500 ms pentru ca servomotorul sa își atingă      
            myservo.detach();             // Detașează servomotorul
            lcd.print("Deschis!");
            delay(1000);
            lastActionMillis = millis();
            break;
          }
        }
         else
         {
            lcd.clear();
            lcd.setCursor(0, 0);
            lcd.print("Pin gresit!");
            delay(1000);
            lcd.clear();
            lcd.print("Mai aveti: ");
            lcd.print(3-i-1);
            lcd.setCursor(0, 1);
            lcd.print("incercari!");
            delay(1000);
            
         }
      }
     lastActionMillis = millis(); 
        
}

void updateLCD()
{
  lcd.setCursor(0, 0); // Seting the cursor on first row 
  lcd.print(line1); // To print line1 message
  lcd.setCursor(0, 1); // Set the cursor to the second row
  int j;
  String s;
  if (offset <= line2.length()) 
  {
    if(offset > line2.length()- 16)
    {
        s = line2.substring(offset, offset + 16 - offset2);
        s = s + line2.substring(0, offset2);
        lcd.print(s); 
        offset2++;
    }
    else
    {
      lcd.print(line2.substring(offset, offset + 16)); // Print the first 16 characters
       // Increment the offset
    }
    offset++; 
  } 
  else 
  {
    offset = 0;
    offset2 = 1;

  }
  delay(400);

}

void Remote_access()
{
  if(pos == 1)
  {
    Serial.write('r');
    char rasp;
    lcd.clear();
    lcd.setCursor(0, 0);
    lcd.print("Se asteapta");
    lcd.setCursor(0, 1);
    lcd.print("Raspunsul!");    
    while(Serial.available() == 0)
    {
      char customKey = customKeypad.getKey();
      if(customKey == 'D')
        break;  
    }
    rasp = Serial.read();
    switch(rasp)
    {
      case 'a':
          lcd.clear();
          lcd.setCursor(0, 0);
          pos = 45;
          myservo.attach(11);  // attaches the servo on pin 9 to the servo object
          myservo.write(pos);              // tell servo to go to position in variable 'pos'
          delay(500);                       // waits 15ms for the servo to reach the positio      
          myservo.detach();             // tell servo to go to position in variable 'pos'
          lcd.print("Acces permis!");
          lcd.setCursor(0, 1);
          lcd.print("Deschis!");
          delay(1000);
          lastActionMillis = millis();
          break;
      case 'd':
          lcd.clear();
          lcd.setCursor(0, 0);
          lcd.print("Acces respins!");
          delay(1000);
          lastActionMillis = millis();
          break;
   }
  }
}

void Schimbare_pin()
{
  Secure_fan = 0;
  char customK;
  int j = 0, i = 0;
  char pin[5] = "";
  char pin1[5] = "";
  char pin2[5] = "";
  lcd.clear();
  lcd.setCursor(0, 0);
  lcd.print("Introduceti");
  lcd.setCursor(0, 1);
  lcd.print("pin: ");
  while(j<4)
  {
    do
    {
      customK = customKeypad.waitForKey();
    }while(isdigit(customK != 0 ));
    lcd.print(customK);
    pin[j] = customK;
    j++;
  }
  j = 0;
  if(strcmp(pin,PIN)== 0)
  {
    lcd.clear();
    lcd.setCursor(0, 0);
    lcd.print("Introduceti noul");
    lcd.setCursor(0, 1);
    lcd.print("pin: ");
    while(j<4)
    {
      do
      {
        customK = customKeypad.waitForKey();
      }while(isdigit(customK != 0 ));
      lcd.print(customK);
      pin1[j] = customK;
      j++;
    }
    j = 0;
    lcd.clear();
    lcd.setCursor(0, 0);
    lcd.print("Introduceti noul");
    lcd.setCursor(0, 1);
    lcd.print("pin iar: ");
    while(j<4)
    {
      do
      {
        customK = customKeypad.waitForKey();
      }while(isdigit(customK != 0 ));
      lcd.print(customK);
      pin2[j] = customK;
      j++;
    }
    if(strcmp(pin1,pin2)== 0)
    {
      Write_Pin_EEPROM(pin1);
      lcd.clear();
      lcd.setCursor(0, 0);
      lcd.print("Pin schimbat!");
      delay(1000);
    }
    else
    {
      lcd.clear();
      lcd.setCursor(0, 0);
      lcd.print("Pinurile introduse");
      lcd.setCursor(0, 1);
      lcd.print("nu se potrivesc.");
      delay(1000);
      lcd.clear();
      lcd.setCursor(0, 0);
      lcd.print("Incercati iara!"); 
      delay(1000);
    }
  }
  else
  {
    Secure_fan = 1;
  }
  lastActionMillis = millis();
}

void AD_Amprenta()
{
  Secure_fan = 0;
  char customK;
  int fan,i,a_id,mem_id = 0;
  int ret_value;
  int fan1;
  int j = 0;
  char pin[5] = "";
  lcd.clear();
  lcd.setCursor(0, 0);
  lcd.print("Introduceti");
  lcd.setCursor(0, 1);
  lcd.print("pin: ");
  while(j<4)
  {
    do
    {
      customK = customKeypad.waitForKey();
    }while(isdigit(customK != 0 ));
    lcd.print(customK);
    pin[j] = customK;
    j++;
  }
  j = 0;
  if(strcmp(pin,PIN)== 0)
  {
  lcd.clear();
  lcd.setCursor(0, 0);
  lcd.print("Alegeti optiunea");
  lcd.setCursor(0, 1);
  lcd.print("1-Add    2-Del");
  do
  {
    customK = customKeypad.waitForKey();
  }while(isDigit((customK != 0 )));
  numar_amprente = readIntFromEEPROM(adresa_nr_amprente);
  if(customK == '1' && numar_amprente < 127)
  {
    numar_amprente++;
    id = numar_amprente;
    Serial.print(id);
    while (! (fan = getFingerprintEnroll()) );
    if(fan == 1)
    {
      lcd.clear();
      lcd.setCursor(0, 0);
      lcd.print("S-a inregistrat");
      lcd.setCursor(0, 1);
      lcd.print("amprenta id: ");
      lcd.print(id);
      writeIntToEEPROM(adresa_nr_amprente, numar_amprente);
    }
    else
    {
      lcd.clear();
      lcd.setCursor(0, 0);
      lcd.print("Nu s-a itreg.");
      lcd.setCursor(0, 1);
      lcd.print("amprenta");
    }
  }
  else if(customK == '2' && numar_amprente > 0)
  {
    lcd.clear();
    lcd.setCursor(0, 0);
    lcd.print("Introduceti id:");
    lcd.setCursor(0, 1);
    fan1 = 1;
    for(i = 0; i < 3; i++)
    {
      do
      {
        customK = customKeypad.waitForKey();
      }while(isDigit((customK != 0 )));
      a_id = charToInt(customK);
      lcd.print(a_id);
      if(a_id != -1)
      {
        mem_id = mem_id * 10 + a_id;// Id-ul amprentei trebuie sa iceapa cu doi de 0 la introducere daca este format dintr-o cifra sau cu un zero daca este format din doua cifre Ex: 001 (pentru amprenta cu Id=ul 1), 021(pentru amprenta cu id-ul 21)
      }
      else
      {
        fan1 = 0;
        break;
      }
    }
    if(fan1 == 1)
    {
      deleteFingerprint(mem_id);
    }
    else
    {
      lcd.clear();
      lcd.setCursor(0, 0);
      lcd.print("Erore la");
      lcd.setCursor(0, 1);
      lcd.print("introducere!");
      delay(2000);
    }
  }
  }
  else
  {
      lcd.clear();
      lcd.setCursor(0, 0);
      lcd.print("Pinul introdus");
      lcd.setCursor(0, 1);
      lcd.print("nu se potriveste.");
      delay(1000);
      Secure_fan = 1;
  }
  lastActionMillis = millis();
}

int charToInt(char tasta)
{
  switch(tasta)
  {
    case '0':
      return 0;
      break;
    case '1':
      return 1;
      break;
    case '2':
      return 2;
      break;
    case '3':
      return 3;
      break;
    case '4':
      return 4;
      break;
    case '5':
      return 5;
      break;
    case '6':
      return 6;
      break;
    case '7':
      return 7;
      break;
    case '8':
      return 8;
      break;
    case '9':
      return 9;
      break;
    default:
      return -1;
      break;
  }
}
//Funcția de scriere în EEPROM a numărului de amprente actualizat
void writeIntToEEPROM(int address, int value) {
  EEPROM.put(address, value);
}

//Funcția de citire din EEPROM a numărului de amprente
int readIntFromEEPROM(int address) {
  int value = 0;
  EEPROM.get(address, value);
  return value;
}

//Pentru addaugarea unei noi amprente

uint8_t getFingerprintEnroll() {

  lcd.clear();
  delay(1000);
  lcd.setCursor(0, 0);
  lcd.print("Apopiati deget");
  int p = -1;
  while (p != FINGERPRINT_OK) {
    p = finger.getImage();
    switch (p) {
    case FINGERPRINT_OK:
    lcd.clear();
    lcd.setCursor(0, 0);
    lcd.print("ok");
    delay(500);
      break;
    case FINGERPRINT_NOFINGER:
      break;
    case FINGERPRINT_PACKETRECIEVEERR:
      break;
    case FINGERPRINT_IMAGEFAIL:
      break;
    default:
      break;
    }
  }
  
  lcd.clear();
  lcd.setCursor(0, 0);
  delay(1000);
  // OK success!

  p = finger.image2Tz(1);
  switch (p) {
    case FINGERPRINT_OK:
    lcd.clear();
    lcd.setCursor(0, 0);
    lcd.print("ok");
    delay(500);
      break;
    case FINGERPRINT_IMAGEMESS:
      return p;
    case FINGERPRINT_PACKETRECIEVEERR:
      return p;
    case FINGERPRINT_FEATUREFAIL:
      return p;
    case FINGERPRINT_INVALIDIMAGE:
      return p;
    default:
      return p;
  }
  lcd.clear();
  lcd.setCursor(0, 0);
  lcd.print("Ridicati deget");
  delay(2000);
  lcd.clear();
  lcd.setCursor(0, 0);
  p = 0;
  while (p != FINGERPRINT_NOFINGER) {
    p = finger.getImage();
  }
  p = -1;
  lcd.print("Pune-ti-l iara");
  while (p != FINGERPRINT_OK) {
    p = finger.getImage();
    switch (p) {
    case FINGERPRINT_OK:
    lcd.clear();
    lcd.setCursor(0, 0);
    lcd.print("ok");
    delay(500);
      break;
    case FINGERPRINT_NOFINGER:
      break;
    case FINGERPRINT_PACKETRECIEVEERR:
      break;
    case FINGERPRINT_IMAGEFAIL:
      break;
    default:
      break;
    }
  }

  lcd.clear();
  lcd.setCursor(0, 0);
  delay(1000);
  // OK success!

  p = finger.image2Tz(2);
  switch (p) {
    case FINGERPRINT_OK:
          lcd.clear();
    lcd.setCursor(0, 0);
    lcd.print("ok");
    delay(500);
      break;
    case FINGERPRINT_IMAGEMESS:
      return p;
    case FINGERPRINT_PACKETRECIEVEERR:
      return p;
    case FINGERPRINT_FEATUREFAIL:
      return p;
    case FINGERPRINT_INVALIDIMAGE:
      return p;
    default:
      return p;
  }

  lcd.clear();
  lcd.setCursor(0, 0);
  delay(1000);
  // OK converted!

  p = finger.createModel();
  if (p == FINGERPRINT_OK) {
    lcd.print("Se potrivesc");
    delay(1000);
  } else if (p == FINGERPRINT_PACKETRECIEVEERR) {
    return p;
  } else if (p == FINGERPRINT_ENROLLMISMATCH) {
    lcd.print("nu se portv!");
    delay(1000);
    return p;
  } else {
    return p;
  }

  lcd.clear();
  lcd.setCursor(0, 0);
  delay(1000);

  p = finger.storeModel(id);
  if (p == FINGERPRINT_OK) {
    lcd.print("Inregistrat!");
    delay(1000);
  } else if (p == FINGERPRINT_PACKETRECIEVEERR) {
    return p;
  } else if (p == FINGERPRINT_BADLOCATION) {
    return p;
  } else if (p == FINGERPRINT_FLASHERR) {
    return p;
  } else {
    return p;
  }

  return 1;
}

uint8_t deleteFingerprint(uint8_t id) {
  uint8_t p = -1;

  p = finger.deleteModel(id);

  if (p == FINGERPRINT_OK) {
      lcd.clear();
      lcd.setCursor(0, 0);
      lcd.print("Amprenta cu id=");
      lcd.setCursor(0, 1);
      lcd.print(id);
      lcd.print(" stearsa");
      delay(1000);
  } else if (p == FINGERPRINT_PACKETRECIEVEERR) {
      lcd.clear();
      lcd.setCursor(0, 0);
      lcd.print("Eroare");
      delay(1000);
  } else if (p == FINGERPRINT_BADLOCATION) {
      lcd.clear();
      lcd.setCursor(0, 0);
      lcd.print("Eroare");
      delay(1000);
  } else if (p == FINGERPRINT_FLASHERR) {
      lcd.clear();
      lcd.setCursor(0, 0);
      lcd.print("Eroare");
      delay(1000);
  } else {
      lcd.clear();
      lcd.setCursor(0, 0);
      lcd.print("Eroare");
      delay(1000);
  }

  return p;
}
